To run the container:

Clone `sloan-srv` repo:

`$ git clone --depth 1 https://uvizhe@bitbucket.org/uvizhe/sloan-srv.git`

Set variables in sloan-srv/src/config.js.

Install build dependencies and build the code:

```
$ cd sloan-srv
$ npm install --only=dev && npm run build
```

Build Docker image:

`$ docker build -t sloan-srv .`

Create Docker volume for a database:

`$ docker volume create sloan-db`

Run a container:

`$ docker run -p 127.0.0.1:3000:3000 --mount source=sloan-db,target=/app/db sloan-srv`
