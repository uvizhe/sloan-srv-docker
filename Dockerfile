FROM node:carbon

WORKDIR /app
COPY sloan-srv/package*.json ./
RUN npm install --only=production
COPY sloan-srv/dist ./

EXPOSE 3000
CMD ["node", "index.js"]
